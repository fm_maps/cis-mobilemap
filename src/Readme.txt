Started with:
ogr2ogr -f GeoJSON -t_srs crs:84 BldgPolygons.geojson BldgPolygons.shp

Project: EPSG:3438 (from prj file)

This says convert your source to EPSG:4326. And EPSG:4326 = WGS84.
https://gis.stackexchange.com/questions/278663/converting-shapefile-to-geojson-utm6-and-mtm3-versions-give-bad-coordinates

Therefore:

ogr2ogr -f GeoJSON -s_srs EPSG:4326 -t_srs EPSG:4326 BldgPolygons.geojson BldgPolygons.shp

This violated right-hand rule.
Try again:

ogr2ogr -f GeoJSON -s_srs EPSG:4326 -t_srs crs:84 BldgPolygons.geojson BldgPolygons.shp

Failed again

Try shape file transform, then to geojason:
ogr2ogr -f "ESRI Shapefile" BldgPolygonswgs84.shp BldgPolygons.shp -s_srs EPSG:4326 -t_srs EPSG:4326
ogr2ogr -f GeoJSON -t_srs crs:84 BldgPolygons.geojson BldgPolygonswgs84.shp


Start over:
(From http://www.gdal.org/drv_geojson.html)
http://www.gdal.org/ogr2ogr.html
ogr2ogr -f GeoJSON -a_srs BldgPolygons.prj -t_srs EPSG:4326 -lco RFC7946=YES BldgPolygons.geojson BldgPolygons.shp

It Worked!!!!!!

Then on to merge:

https://www.datavizforall.org/transform/mapshaper/#join-spreadsheet-data-with-polygon-map

Go to https://mapshaper.org/
load buildings.csv first
Load BldgPolygons.geojson second
type in console:  	-join buildings.csv keys=Building,Building
Click on "i", then click buildings to verify
Then Export to GeoJSON - "output.zip"
Extract BulidingPolygons.json - rename to MergedBuildingsPolygons.geojson

